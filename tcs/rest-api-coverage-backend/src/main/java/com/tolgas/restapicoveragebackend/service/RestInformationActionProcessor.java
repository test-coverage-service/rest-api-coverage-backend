package com.tolgas.restapicoveragebackend.service;

import com.tolgas.restapicoveragebackend.model.document.RestInformationAction;
import com.tolgas.restapicoveragebackend.model.document.RestRequest;
import com.tolgas.restapicoveragebackend.model.document.RestResponse;
import com.tolgas.restapicoveragebackend.repository.RestInformationActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class RestInformationActionProcessor {

    @Autowired
    private RestInformationActionRepository restInformationActionRepository;
    private RestTemplate restTemplate;

    private Logger logger;

    public void process(Long id) {
        logger.log(Level.INFO, "Starting process for Rest information action...");

        RestInformationAction restInformationAction = restInformationActionRepository.findById(id).get();

        RestRequest testRequest = restInformationAction.getRequest();

        RequestEntity<?> request = testRequest.toRequestEntity();

        ResponseEntity<Object> actualResponseEntity = restTemplate.exchange(request, Object.class);

        RestResponse expectedResponse = restInformationAction.getResponse();

        boolean isExpectedAndActualEquals =
                expectedResponse.getCode() == actualResponseEntity.getStatusCode().value()
                        && expectedResponse.getBody().equals(actualResponseEntity.getBody());

        restInformationActionRepository.save(restInformationAction);
    }
}
