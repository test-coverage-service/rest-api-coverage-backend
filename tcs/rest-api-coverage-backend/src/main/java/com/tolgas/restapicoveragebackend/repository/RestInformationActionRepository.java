package com.tolgas.restapicoveragebackend.repository;


import com.tolgas.restapicoveragebackend.model.document.RestInformationAction;
import org.springframework.data.repository.CrudRepository;

public interface RestInformationActionRepository extends CrudRepository<RestInformationAction, Long> {
    RestInformationAction save(RestInformationAction restInformationAction);
}
