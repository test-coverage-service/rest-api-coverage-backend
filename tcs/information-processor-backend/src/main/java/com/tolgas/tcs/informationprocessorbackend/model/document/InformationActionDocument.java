package com.tolgas.tcs.informationprocessorbackend.model.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@Document
@AllArgsConstructor
public class InformationActionDocument {

    @MongoId
    private Long id;
    private Long actionId;
    private ActionType actionType;

    public enum ActionType {
        GRAPHQL, REST
    }

    private ProcessResult processResult;

    public enum ProcessResult {
        SUCCESS, FAILED
    }
}
