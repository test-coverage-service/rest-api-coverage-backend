package com.tolgas.tcs.informationprocessorbackend.service;

import com.tolgas.tcs.informationprocessorbackend.integration.GraphQIntegrationService;
import com.tolgas.tcs.informationprocessorbackend.integration.RestIntegrationService;
import com.tolgas.tcs.informationprocessorbackend.model.document.InformationActionDocument;
import com.tolgas.tcs.informationprocessorbackend.model.document.InformationProcessDocument;
import com.tolgas.tcs.informationprocessorbackend.repository.InformationProcessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class InformationProcessService {
    private Logger logger;

    @Autowired
    private InformationProcessRepository informationProcessRepository;

    @Autowired
    private GraphQIntegrationService graphQIntegrationService;

    @Autowired
    private RestIntegrationService restIntegrationService;

    public void process(Long id) {
        logger.log(Level.INFO, "Information process starting... ");

        InformationProcessDocument informationProcess = informationProcessRepository.findById(id).get();

        List<InformationActionDocument> actions = informationProcess.getActions();

        if (CollectionUtils.isEmpty(actions)) {
            throw new IllegalArgumentException("Test actions cannot be empty");
        }

        for (InformationActionDocument testAction : actions) {
            switch (testAction.getActionType()) {
                case REST -> restIntegrationService.process(testAction);
                case GRAPHQL -> graphQIntegrationService.process(testAction);
            }
        }
    }
}
