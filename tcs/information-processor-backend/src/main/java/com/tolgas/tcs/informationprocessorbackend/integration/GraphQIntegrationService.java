package com.tolgas.tcs.informationprocessorbackend.integration;

import com.tolgas.tcs.informationprocessorbackend.model.document.InformationActionDocument;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class GraphQIntegrationService {

    RestTemplate restTemplate = new RestTemplate();

    public void process(InformationActionDocument informationActionDocument) {
        restTemplate.postForEntity("http://localhost:8081/graphqlAction/{" + informationActionDocument.getActionId() + "}/process", "", Void.class);
    }
}
